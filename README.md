# Paper Pig

Paper Pig is a 2D animated sequence made with Blender Grease Pencil. It's made to be a demo scene for research around 2D animation. Anyone can use it to learn or test new tools on it, and share demos.

![](docs/paper_pig.mp4)

## Content
The file contains 5 grease pencil objects, and a total of 24 layers. The scene also contains a hidden grease pencil object with the rough version of the animation.

![UI](docs/paper_pig_scene.png "Paper Pig scene overview")

## Credits
Les Fées Spéciales

 - Design, Set, Animation : Julien Delwaulle
 - Technical Director : Flavio Perez
 - Developers  : Amélie Fondevilla, Damien Picard
 - Production Manager : Natalène Darfeuille

This scene is part of the our RnD program "Grease Pencil Powertools", made possible with the support of the CNC.

## License
Paper Pig © 2023 by Les Fées Spéciales is licensed under CC BY 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

This license enables reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. CC BY includes the following elements:

CC BY: Les Fées Spéciales.